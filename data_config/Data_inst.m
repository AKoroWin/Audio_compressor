function [Fs,Data_fs,time,multi] = Data_inst(Data)

Data_inter = interp(Data,22);
Data_decim = decimate(Data_inter,25);
Fs = 39062.5;
Data_fs = Data_decim;
time = (0:length(Data_fs)-1) / Fs;
multi = 10^(20/20);

end

