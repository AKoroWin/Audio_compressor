function [audioData] = Data_read(filename)
frameLength = 1024;

fileReader = dsp.AudioFileReader('Filename',filename, 'SamplesPerFrame', frameLength);

audioData = [];
while ~isDone(fileReader)
    audioFrame = fileReader();
    audioData = [audioData; audioFrame(:, 1)]; % Чтение только первого канала
end

end

