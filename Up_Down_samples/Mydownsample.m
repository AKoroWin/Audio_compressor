function [Data_out] = Mydownsample(Data_in)
Data_out = zeros(1,length(Data_in)/2);
idx = 1;
    for i = 1:length(Data_in)
        if mod(i,2) ~= 0
            Data_out(idx)  = Data_in(i);
            idx = idx + 1;
        end
    end
end


