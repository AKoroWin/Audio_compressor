onbreak resume
onerror resume
vsim -voptargs=+acc work.Limiter_func_for_HDL_fixpt_tb

add wave sim:/Limiter_func_for_HDL_fixpt_tb/u_Limiter_func_for_HDL_fixpt/Data_in
add wave sim:/Limiter_func_for_HDL_fixpt_tb/u_Limiter_func_for_HDL_fixpt/threshold
add wave sim:/Limiter_func_for_HDL_fixpt_tb/u_Limiter_func_for_HDL_fixpt/Data_out
add wave sim:/Limiter_func_for_HDL_fixpt_tb/Data_out_ref
run -all
