-- -------------------------------------------------------------
-- 
-- File Name: D:\audio_compressor_YADRO\convert_HDL_code\src.mat\codegen\Limiter_func_for_HDL\hdlsrc\Limiter_func_for_HDL_fixpt_tb.vhd
-- Created: 2024-04-19 08:17:51
-- 
-- Generated by MATLAB 9.9, MATLAB Coder 5.1 and HDL Coder 3.17
-- 
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Model base rate: 1
-- Target subsystem base rate: 1
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: Limiter_func_for_HDL_fixpt_tb
-- Source Path: 
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_textio.ALL;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
LIBRARY STD;
USE STD.textio.ALL;
USE work.Limiter_func_for_HDL_fixpt_tb_pkg.ALL;

ENTITY Limiter_func_for_HDL_fixpt_tb IS
END Limiter_func_for_HDL_fixpt_tb;


ARCHITECTURE rtl OF Limiter_func_for_HDL_fixpt_tb IS

  -- Component Declarations
  COMPONENT Limiter_func_for_HDL_fixpt
    PORT( Data_in                         :   IN    std_logic_vector(1 DOWNTO 0);  -- ufix2
          threshold                       :   IN    std_logic_vector(1 DOWNTO 0);  -- ufix2
          Data_out                        :   OUT   std_logic_vector(1 DOWNTO 0)  -- ufix2
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : Limiter_func_for_HDL_fixpt
    USE ENTITY work.Limiter_func_for_HDL_fixpt(rtl);

  -- Signals
  SIGNAL clk                              : std_logic;
  SIGNAL reset                            : std_logic;
  SIGNAL enb                              : std_logic;
  SIGNAL Data_out_addr                    : std_logic;  -- ufix1
  SIGNAL Data_out_lastAddr                : std_logic;  -- ufix1
  SIGNAL check1_done                      : std_logic;  -- ufix1
  SIGNAL snkDonen                         : std_logic;
  SIGNAL resetn                           : std_logic;
  SIGNAL tb_enb                           : std_logic;
  SIGNAL rdEnb                            : std_logic;
  SIGNAL Data_out_done                    : std_logic;  -- ufix1
  SIGNAL Data_out_done_enb                : std_logic;  -- ufix1
  SIGNAL rawData_Data_in                  : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL holdData_Data_in                 : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL rawData_threshold                : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL holdData_threshold               : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL ce_out                           : std_logic;
  SIGNAL Data_in_offset                   : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL Data_in_1                        : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL Data_in_2                        : std_logic_vector(1 DOWNTO 0);  -- ufix2
  SIGNAL threshold_offset                 : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL threshold_1                      : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL threshold_2                      : std_logic_vector(1 DOWNTO 0);  -- ufix2
  SIGNAL Data_out                         : std_logic_vector(1 DOWNTO 0);  -- ufix2
  SIGNAL Data_out_unsigned                : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL Data_out_expected_1              : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL Data_out_ref                     : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL Data_out_testFailure             : std_logic;  -- ufix1

BEGIN
  u_Limiter_func_for_HDL_fixpt : Limiter_func_for_HDL_fixpt
    PORT MAP( Data_in => Data_in_2,  -- ufix2
              threshold => threshold_2,  -- ufix2
              Data_out => Data_out  -- ufix2
              );

  Data_out_lastAddr <= '1';

  snkDonen <=  NOT check1_done;

  tb_enb <= resetn AND snkDonen;

  
  rdEnb <= tb_enb WHEN check1_done = '0' ELSE
      '0';

  Data_out_done_enb <= Data_out_done AND rdEnb;

  clk_gen: PROCESS 
  BEGIN
    clk <= '1';
    WAIT FOR 5 ns;
    clk <= '0';
    WAIT FOR 5 ns;
    IF check1_done = '1' THEN
      clk <= '1';
      WAIT FOR 5 ns;
      clk <= '0';
      WAIT FOR 5 ns;
      WAIT;
    END IF;
  END PROCESS clk_gen;

  reset_gen: PROCESS 
  BEGIN
    reset <= '1';
    WAIT FOR 20 ns;
    WAIT UNTIL clk'event AND clk = '1';
    WAIT FOR 2 ns;
    reset <= '0';
    WAIT;
  END PROCESS reset_gen;

  resetn <=  NOT reset;

  Data_out_done <= Data_out_lastAddr AND resetn;

  -- Delay to allow last sim cycle to complete
  checkDone_1_process: PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      check1_done <= '0';
    ELSIF clk'event AND clk = '1' THEN
      IF Data_out_done_enb = '1' THEN
        check1_done <= Data_out_done;
      END IF;
    END IF;
  END PROCESS checkDone_1_process;

  enb <= rdEnb AFTER 2 ns;

  -- Data source for Data_in
  rawData_Data_in <= to_unsigned(16#3#, 2);

  -- holdData reg for Data_in
  stimuli_Data_in_process: PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      holdData_Data_in <= (OTHERS => 'X');
    ELSIF clk'event AND clk = '1' THEN
      holdData_Data_in <= rawData_Data_in;
    END IF;
  END PROCESS stimuli_Data_in_process;

  -- Data source for threshold
  rawData_threshold <= to_unsigned(16#2#, 2);

  -- holdData reg for threshold
  stimuli_threshold_process: PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      holdData_threshold <= (OTHERS => 'X');
    ELSIF clk'event AND clk = '1' THEN
      holdData_threshold <= rawData_threshold;
    END IF;
  END PROCESS stimuli_threshold_process;

  ce_out <= enb AND (rdEnb AND tb_enb);

  stimuli_Data_in_1: PROCESS (rawData_Data_in, rdEnb)
  BEGIN
    IF rdEnb = '0' THEN
      Data_in_offset <= holdData_Data_in;
    ELSE
      Data_in_offset <= rawData_Data_in;
    END IF;
  END PROCESS stimuli_Data_in_1;

  Data_in_1 <= Data_in_offset AFTER 2 ns;

  Data_in_2 <= std_logic_vector(Data_in_1);

  stimuli_threshold_1: PROCESS (rawData_threshold, rdEnb)
  BEGIN
    IF rdEnb = '0' THEN
      threshold_offset <= holdData_threshold;
    ELSE
      threshold_offset <= rawData_threshold;
    END IF;
  END PROCESS stimuli_threshold_1;

  threshold_1 <= threshold_offset AFTER 2 ns;

  threshold_2 <= std_logic_vector(threshold_1);

  Data_out_unsigned <= unsigned(Data_out);

  -- Data source for Data_out_expected
  Data_out_expected_1 <= to_unsigned(16#2#, 2);

  Data_out_ref <= Data_out_expected_1;

  Data_out_unsigned_checker: PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Data_out_testFailure <= '0';
    ELSIF clk'event AND clk = '1' THEN
      IF ce_out = '1' AND Data_out_unsigned /= Data_out_ref THEN
        Data_out_testFailure <= '1';
        ASSERT FALSE
          REPORT "Error in Data_out_unsigned: Expected " & to_hex(Data_out_ref) & (" Actual " & to_hex(Data_out_unsigned))
          SEVERITY ERROR;
      END IF;
    END IF;
  END PROCESS Data_out_unsigned_checker;

  completed_msg: PROCESS (clk)
  BEGIN
    IF clk'event AND clk = '1' THEN
      IF check1_done = '1' THEN
        IF Data_out_testFailure = '0' THEN
          ASSERT FALSE
            REPORT "**************TEST COMPLETED (PASSED)**************"
            SEVERITY NOTE;
        ELSE
          ASSERT FALSE
            REPORT "**************TEST COMPLETED (FAILED)**************"
            SEVERITY NOTE;
        END IF;
      END IF;
    END IF;
  END PROCESS completed_msg;

END rtl;

