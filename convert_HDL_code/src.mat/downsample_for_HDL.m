function [Data_out] = downsample_for_HDL(Data_in)
Data_out = zeros(1,length(Data_in)/2);
idx = 1;
    for i = 1:2:length(Data_in)
        
            Data_out(idx)  = Data_in(i);
            idx = idx + 1;
        
    end
end

