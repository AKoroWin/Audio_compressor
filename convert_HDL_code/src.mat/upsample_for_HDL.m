function Data_out = upsample_for_HDL(Data_in)
    Data_out = zeros(1,2*length(Data_in));
    idx = 1;
    for i = 1:length(Data_in)
        Data_out(idx) = Data_in(i);
        idx = idx + 2;
    end
end
