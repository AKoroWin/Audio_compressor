function [Data_out] = Limiter_func_for_HDL(Data_in,threshold)
    if Data_in > threshold
        Data_out = threshold;
    elseif Data_in < -threshold
        Data_out = -threshold;
    else
        Data_out = Data_in; 
    end
end

