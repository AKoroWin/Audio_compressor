-- -------------------------------------------------------------
-- 
-- File Name: D:\audio.comp.YADRO\convert_HDL_code\codegen\Limiter_func_for_HDL\hdlsrc\Limiter_func_for_HDL_fixpt.vhd
-- Created: 2024-04-18 14:11:03
-- 
-- Generated by MATLAB 9.9, MATLAB Coder 5.1 and HDL Coder 3.17
-- 
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Design base rate: 1
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: Limiter_func_for_HDL_fixpt
-- Source Path: Limiter_func_for_HDL_fixpt
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY Limiter_func_for_HDL_fixpt IS
  PORT( Data_in                           :   IN    std_logic_vector(1 DOWNTO 0);  -- ufix2
        threshold                         :   IN    std_logic_vector(1 DOWNTO 0);  -- ufix2
        Data_out                          :   OUT   std_logic_vector(1 DOWNTO 0)  -- ufix2
        );
END Limiter_func_for_HDL_fixpt;


ARCHITECTURE rtl OF Limiter_func_for_HDL_fixpt IS

  -- Signals
  SIGNAL Data_in_unsigned                 : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL threshold_unsigned               : unsigned(1 DOWNTO 0);  -- ufix2
  SIGNAL Data_out_tmp                     : unsigned(1 DOWNTO 0);  -- ufix2

BEGIN
  Data_in_unsigned <= unsigned(Data_in);

  threshold_unsigned <= unsigned(threshold);

  --HDL code generation from MATLAB function: Limiter_func_for_HDL_fixpt
  --F2F: End block 
  --F2F: Start block
  --F2F: No information found for converting the following block of code
  --%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  --                                                                          %
  --           Generated by MATLAB 9.9 and Fixed-Point Designer 7.1           %
  --                                                                          %
  --%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  Data_out_tmp <= threshold_unsigned WHEN Data_in_unsigned > threshold_unsigned ELSE
      Data_in_unsigned;

  Data_out <= std_logic_vector(Data_out_tmp);

END rtl;

