function [Data_out] = Limiter_func(Data_in,threshold)
Data_out = Data_in;
Data_out(Data_in > threshold) = threshold;
Data_out(Data_in < -threshold) = -threshold;
end

