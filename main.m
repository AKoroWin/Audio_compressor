% mkdir('filter_init');
% mkdir('data_config');
% mkdir('hd');
% mkdir('limiter');
% mkdir('convert_HDL_code')
% mkdir('Up_Down_samples')
%mkdir('FPGA_project')
close all;clear;clc;
addpath('filter_init','data_config','hd','limiter','Up_Down_samples');
%inst filter
Lowpass = Lowpass_inst();
Bandpass = Bandpass_inst();

Data = Data_read('mister-anderson.wav');

[Fs,Data_fs,time,multi] = Data_inst(Data);

Data_fs = filter(Lowpass.Numerator,1,Data_fs);

%transport spectrum
Data_af_hd = HD(Data_fs);
Data_af_hd = Myupsample(Data_af_hd);
Data_af_hd = filter(Bandpass.Numerator,1,Data_af_hd);

%test path for Limiter_func_HDL
% Data_af_lim = [];
% %Limiter transfomation
% for i = 1:size(Data_af_hd)
% Data_af_lim(i) = Limiter_func_HDL(Data_af_hd(i),0.02);
% end

%Limiter transfomation
Data_af_lim = Limiter_func(Data_af_hd,0.02);

%return spectrum
Data_bef_ret = filter(Bandpass.Numerator,1,Data_af_lim);
Data_bef_ret = Mydownsample(Data_bef_ret);

Data_return = HD(Data_bef_ret);
Data_return = filter(Lowpass.Numerator,1,Data_return);

Data_return = Data_return .* multi;

PAR_original = max(Data_fs)/rms(Data_fs);
disp(PAR_original);
PAR_lim = max(Data_return)/rms(Data_return);
disp(PAR_lim);

%sound(Data_return,Fs);


 





